#include "stdafx.h"
#include "gtest/gtest.h"
#include "UnzipPptxInterface.h"
#include "TestUtils.h"
#include "Shlwapi.h"
#include "Shellapi.h"
#include "Windows.h"
#include "sys/stat.h"

bool isDirEmpty(const std::wstring& pathToOutDir)
{
	return PathIsDirectoryEmpty(pathToOutDir.c_str());
}

int silently_remove_directory(LPCTSTR dir) // Fully qualified name of the directory being deleted, without trailing backslash
{
    SHFILEOPSTRUCT file_op = {
        NULL,
        FO_DELETE,
        dir,
        NULL,
        FOF_NOCONFIRMATION |
        FOF_NOERRORUI |
        FOF_SILENT,
        false,
        0,
        NULL };
    int a = SHFileOperation(&file_op);
	return a;
}

TEST(zLibTest, isFolderEmpty)
{
	std::wstring pathToTestFile = createRelatedPath(L"TestFiles\\testFile.pptx");
	std::wstring pathToOutDir = createRelatedPath(L"TestFiles\\outDir");
	unzipPptx(pathToTestFile.c_str(), pathToOutDir.c_str());
	bool b = isDirEmpty(pathToOutDir);
	pathToOutDir.push_back('\0');
	int value = silently_remove_directory(pathToOutDir.c_str());
	ASSERT_FALSE(b);
}

