﻿#include "stdafx.h"
#include "gtest/gtest.h"
#include "IXmlParser.h"
#include <windows.h>
#include <iostream>
#include "TestUtils.h"
#include "PugiXmlParser.h"
#include <iostream>
#include <memory>

TEST(parseXml, PugiXmlUnicodeTextRus)
{
	std::auto_ptr<IXmlParser> parser(IXmlParser::createXmlParser(pugiXml));
	std::vector<std::wstring> testVector;	
	std::vector<std::wstring> testVector1;
	std::wstring word = L"Юникод";
	std::wstring pathToTestXml = createRelatedPath(L"TestFiles\\slide6.xml");
	parser -> parseXml(pathToTestXml, testVector, testVector1);
	ASSERT_TRUE(testVector[0] == word);
}

TEST(parseXml, PugiXmlContainImages)
{
	std::auto_ptr<IXmlParser> parser(IXmlParser::createXmlParser(pugiXml));
	std::vector<std::wstring> testVector;	
	std::vector<std::wstring> testVector1;
	std::wstring word = L"../media/image1.jpg";
	std::wstring pathToTestXml = createRelatedPath(L"TestFiles\\slide7.xml.rels");
	parser -> parseXml(pathToTestXml, testVector, testVector1);
	ASSERT_TRUE(testVector1[0] == word);
}

TEST(parseXml, IsExtractedResultTrueTest)
{
	std::auto_ptr<IXmlParser> parser(IXmlParser::createXmlParser(tinyXml));
	std::vector<std::wstring> testVector;	
	std::vector<std::wstring> testVector1;
	std::wstring word = L"guitar";
	std::wstring pathToTestXml = createRelatedPath(L"TestFiles\\slide1.xml");
	parser -> parseXml(pathToTestXml, testVector, testVector1);
	ASSERT_TRUE(*testVector.begin() == word);
}

TEST(parseXml, EmptyDocumentTest)
{
	std::auto_ptr<IXmlParser> parser(IXmlParser::createXmlParser(tinyXml));
	std::vector<std::wstring> testVector;	
	std::vector<std::wstring> testVector1;
	std::wstring pathToTestXml = createRelatedPath(L"TestFiles\\slide3.xml");
	parser -> parseXml(pathToTestXml, testVector, testVector1);
	ASSERT_TRUE(testVector.size() == 0);
}

TEST(parseXml, TinyXmlContainImages)
{
	std::auto_ptr<IXmlParser> parser(IXmlParser::createXmlParser(tinyXml));
	std::vector<std::wstring> testVector;	
	std::vector<std::wstring> testVector1;
	std::wstring word = L"../media/image1.jpg";
	std::wstring pathToTestXml = createRelatedPath(L"TestFiles\\slide7.xml.rels");
	parser -> parseXml(pathToTestXml, testVector, testVector1);
	ASSERT_TRUE(testVector1[0] == word);
}

TEST(parseXml, TinyXmlIsExtractedResultTrueTest)
{
	std::auto_ptr<IXmlParser> parser(IXmlParser::createXmlParser(tinyXml));
	std::vector<std::wstring> testVector;	
	std::vector<std::wstring> testVector1;
	std::wstring word = L"guitar";
	std::wstring pathToTestXml = createRelatedPath(L"TestFiles\\slide1.xml");
	parser -> parseXml(pathToTestXml, testVector, testVector1);
	ASSERT_TRUE(*testVector.begin() == word);
}

TEST(parseXml, TinyXmlEmptyDocumentTest)
{
	std::auto_ptr<IXmlParser> parser(IXmlParser::createXmlParser(tinyXml));
	std::vector<std::wstring> testVector;	
	std::vector<std::wstring> testVector1;
	std::wstring pathToTestXml = createRelatedPath(L"TestFiles\\slide3.xml");
	parser -> parseXml(pathToTestXml, testVector, testVector1);
	ASSERT_TRUE(testVector.size() == 0);
}