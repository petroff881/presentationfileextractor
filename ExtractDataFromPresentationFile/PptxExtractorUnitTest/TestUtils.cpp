#include "stdafx.h"
#include "TestUtils.h"
#include <windows.h>

std::wstring exePath() 
{
    wchar_t buffer[MAX_PATH];
    GetModuleFileNameW( NULL, buffer, MAX_PATH );
	std::wstring www(buffer);
	return www;
}

std::wstring createRelatedPath(std::wstring projPath)
{
	std::wstring currentPath = exePath();
	size_t b = currentPath.find(L"bin");
	if (b != std::wstring::npos)
	{
		std::wstring result;
		result = currentPath + L"\\..\\..\\..\\..\\PptxExtractorUnitTest\\" + projPath;
		return result;
	}
	else 
	{
		std::wstring result;
		result = currentPath + L"\\" + projPath;
		return result;
	}
}