#include "extractorgui.h"
#include "ImageLabel.h"
#include <QCloseEvent>

std::wstring getWDirectoryPath(const QString& str)
{
	std::wstring pathToFile = str.toStdWString ();
	std::wstring::reverse_iterator it = pathToFile.rbegin();
	while(*it != L'/')
	{
		pathToFile.pop_back();
		++it;
	}	
	return pathToFile.append(L"tmp");
}

QString getImageName(std::wstring& pathToImage )
{
	int pos = pathToImage.find_last_of(L"/");
	pathToImage.erase(0, pos);
	return QString::fromStdWString(pathToImage);
}

std::string& changeExtension(std::string& pathToImage)
{
	int pos = pathToImage.find_last_of(".");
	pathToImage.erase(pos + 1, *pathToImage.rbegin());		
	pathToImage.append("md5");
	return pathToImage;
}

void removeDir(QString& rootPath, QString& pptxFileName)
{
	QDir dir(rootPath);
	dir.removeRecursively();
	pptxFileName.clear();
	rootPath.clear();
}

void cleanLayout(QBoxLayout* widget)
{
	QLayoutItem* item;
	while ( ( item = widget->layout()->takeAt( 0 ) ) != NULL )
	{
		delete item->widget();
		delete item;
	}
}

ExtractorGui::ExtractorGui(QWidget *parent)
	: QMainWindow(parent), slidesAmount(0), parser(IXmlParser::createXmlParser(pugiXml))
{
	ui.setupUi(this);
	ui.actionExport_all ->setEnabled(false);
}

ExtractorGui::~ExtractorGui()
{

}

void ExtractorGui::on_action_Open_triggered()
{
	if(pptxFileName.size())
	{
		ui.textBrowser->clear();
		cleanLayout(ui.horizontalAAA);
		cleanLayout(ui.verticalLayout_2);
		removeDir(rootPath, pptxFileName);
		slidesAmount = 0;
		textContainer.empty();
		imagesContainer.empty();
	}

	pptxFileName = QFileDialog::getOpenFileName(this, tr("Open File"), QString(),
		tr("Presentation Files (*.pptx)"));
	if (!pptxFileName.isEmpty())
	{
		QFile file(pptxFileName);
		if (!file.open(QIODevice::ReadOnly))
		{
			QMessageBox::critical(this, tr("Error"), tr("Could not open file"));
			return;
		}
		ui.actionExport_all->setEnabled(true);
		unzipPptx((pptxFileName.toStdWString ()).c_str(), getWDirectoryPath(pptxFileName).c_str());
		rootPath = QString::fromWCharArray(getWDirectoryPath(pptxFileName).c_str());
		QFileSystemModel *TreeModel = new QFileSystemModel();
		TreeModel->setRootPath(rootPath);
		ui.treeView->setModel(TreeModel);
		QModelIndex idx = TreeModel->index(rootPath);
		ui.treeView->setRootIndex(idx);
		if(QDir(rootPath + "/ppt/slides").exists())
		{
			QDirIterator it(rootPath + "/ppt/slides", QStringList() << "*.xml", QDir::Files, QDirIterator::Subdirectories);
			while (it.hasNext())
			{
				++slidesAmount;
				it.next();
			}
		}
		else
		{
			return;
		}
		for (int i = 0; i < slidesAmount; ++i)
		{
			QPushButton* btn = new QPushButton;
			QSize buttonSize(128,90);
			btn->setFixedSize(buttonSize);
			btn -> setText("Slide " + QString::number(i + 1));
			ui.horizontalAAA -> insertWidget(-1, btn, 0, 0);
			QSignalMapper* signalMapper = new QSignalMapper(this);
			QSignalMapper* signalMapper1 = new QSignalMapper(this);
			signalMapper->setMapping(btn, rootPath + "/ppt/slides/slide" + QString::number(i+1) + ".xml");
			signalMapper1->setMapping(btn, rootPath + "/ppt/slides/_rels/slide" + QString::number(i+1) + ".xml.rels");
			connect(btn, SIGNAL(clicked()), signalMapper, SLOT (map()));
			connect(btn, SIGNAL(clicked()), signalMapper1, SLOT (map()));
			connect(signalMapper, SIGNAL(mapped(const QString &)), this, SLOT(displayText(const QString &)));
			connect(signalMapper1, SIGNAL(mapped(const QString &)), this, SLOT(displayImages(const QString &)));
		}	
	}
}

void ExtractorGui::on_action_Close_triggered()
{
	if(pptxFileName.isEmpty())
	{
		close();
	}
	else
	{
		removeDir(rootPath, pptxFileName);
		close();
	}
}

void ExtractorGui::displayText(const QString& pathToXml)
{
	textContainer.clear();
	ui.textBrowser->clear();
	parser -> parseXml(pathToXml.toStdWString(), textContainer, imagesContainer);
	std::vector<std::wstring>::iterator it = textContainer.begin();
	for (it; it != textContainer.end(); ++it)
	{
		ui.textBrowser->append(QString::fromStdWString(*it));
	}
}

void ExtractorGui::displayImages(const QString& pathToXml)
{
	cleanLayout(ui.verticalLayout_2);
	imagesContainer.clear();
	parser -> parseXml(pathToXml.toStdWString(), textContainer, imagesContainer);
	std::vector<std::wstring>::iterator it = imagesContainer.begin();
	for (it; it != imagesContainer.end(); ++it)
	{
		if ((*it).find(L"media/image") != std::wstring::npos)
		{
			ImageLabel* lbl = new ImageLabel;
			QPixmap pixmap(rootPath + "/ppt/slides/" + QString::fromStdWString(*it));	
			lbl->setPixmap(pixmap);
			//ui.verticalLayout_2 -> insertWidget(-1, lbl, 0, 0);
			ui.verticalLayout_2 -> addWidget(lbl);
		}
	}
}

void ExtractorGui::on_actionExport_all_triggered()
{
	QString path = QFileDialog::getExistingDirectory
		(this, tr("Open Directory"),"/home",
		QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	QDir dir = QDir::root();
	QString dirName = path + "/ExportedFilesFromPptx";
	QString directory = dirName;
	int i = 1;
	bool dirExists = QDir(dirName).exists();
	while(dirExists)
	{
		directory = dirName;
		directory += QString::number(i);
		dirExists = QDir(directory).exists();
		++i;
	}
	dir.mkdir(directory);
	imagesContainer.clear();
	textContainer.clear();
	for(int i = 0; i < slidesAmount; ++i)
	{
		dir.mkdir(directory + "/slide" + QString::number(i + 1));
		parser -> parseXml((rootPath + "/ppt/slides/slide" + QString::number(i+1) + ".xml").toStdWString(), textContainer, imagesContainer);
		parser -> parseXml((rootPath + "/ppt/slides/_rels/slide" + QString::number(i+1) + ".xml.rels").toStdWString(), textContainer, imagesContainer);
		QString filename = directory + "/slide" + QString::number(i + 1) +"/slide" + QString::number(i + 1) + ".txt";
		if(textContainer.size() != 0)
		{
			QFile file(filename);
			if (file.open(QIODevice::ReadWrite))
			{
				QTextStream stream(&file);
				stream.setCodec("UTF-8");
				std::vector<std::wstring>::iterator it = textContainer.begin();
				for (it; it != textContainer.end(); ++it)
				{
					stream << QString::fromStdWString(*it) << "\r\n";
				}
				file.close();
				QFile* file = new QFile(filename);
				if(file->open(QIODevice::ReadOnly))
				{
					QCryptographicHash* hash = 0;
					QByteArray rrr = file->readAll();
					QByteArray result = hash->hash(rrr,QCryptographicHash::Md5);
					std::string str = QString(result.toHex()).toStdString();
					file->close();
					QString md5Filename = directory + "/slide" + QString::number(i + 1) +"/slide" + QString::number(i + 1)  + ".md5";
					QFile file(md5Filename);
					if (file.open(QIODevice::ReadWrite))
					{
						QTextStream stream(&file);
						stream << QString::fromUtf8(str.c_str());
						file.close();
					}  
				}
			}
		}
		if(!imagesContainer.empty())
		{
			std::vector<std::wstring>::iterator it = imagesContainer.begin();
			for (it; it != imagesContainer.end(); ++it)
			{
				if ((*it).find(L"media/image") != std::string::npos)
				{
					QString base =rootPath + "/ppt/slides/" + QString::fromStdWString(*it);
					QString dst = directory + "/slide" + QString::number(i + 1) + getImageName(*it);
					QFile::copy(base, dst);
					std::string ext = changeExtension(dst.toStdString());
					QString md5Filename = QString::fromUtf8(ext.c_str());
					QFile* file = new QFile(dst);
					if(file->open(QIODevice::ReadOnly))
					{
						QCryptographicHash* hash = 0;
						QByteArray rrr = file->readAll();
						QByteArray result = hash->hash(rrr,QCryptographicHash::Md5);
						std::string str = QString(result.toHex()).toStdString();
						file->close();
						QFile file(md5Filename);
						if (file.open(QIODevice::ReadWrite))
						{
							QTextStream stream(&file);
							stream << QString::fromUtf8(str.c_str());
							file.close();
						}  
					}
				}
			}
		}
		imagesContainer.clear();
		textContainer.clear();
	}
}

void ExtractorGui::closeEvent (QCloseEvent *event)
{
	if(pptxFileName.size())
	{
		ui.textBrowser->clear();
		cleanLayout(ui.horizontalAAA);
		cleanLayout(ui.verticalLayout_2);
		removeDir(rootPath, pptxFileName);
		slidesAmount = 0;
		textContainer.empty();
		imagesContainer.empty();
	}
	event->accept();
}
