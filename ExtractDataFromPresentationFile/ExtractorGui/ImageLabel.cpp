#include "ImageLabel.h"
#include <QDialog>
#include <QHBoxLayout>

ImageLabel::ImageLabel(QWidget *parent) :
	QWidget(parent)
{
    label = new QLabel(this);
    label->setScaledContents(true);
    QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

void ImageLabel::resizeEvent(QResizeEvent *event) {
    QWidget::resizeEvent(event);
    resizeImage();
}

const QPixmap* ImageLabel::pixmap() const {
    return label->pixmap();
}

void ImageLabel::setPixmap (const QPixmap &pixmap){
    m_pixmap = new QPixmap(pixmap);
    label->setPixmap(pixmap);
    resizeImage();
}

void ImageLabel::resizeImage() {
    QSize pixSize = label->pixmap()->size();
    QSize s = size();
    pixSize.scale(s, Qt::KeepAspectRatio);
    label->setMinimumSize(pixSize);
    label->setFixedSize(pixSize);
}

void ImageLabel::mousePressEvent(QMouseEvent* event)
{
    QDialog* popUp = new QDialog(this);
    QHBoxLayout* layout = new QHBoxLayout();
    QLabel* lab = new QLabel();
    lab->setPixmap(*m_pixmap);
    layout->addWidget(lab);
    popUp->setLayout(layout);
    popUp->exec();
}