#ifndef EXTRACTORGUI_H
#define EXTRACTORGUI_H

#include <QtWidgets/QMainWindow>
#include "ui_extractorgui.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QFileSystemModel>
#include "UnzipPptxInterface.h"
#include <memory>
#include "Shlwapi.h"
#include <string>
#include <stdio.h>
#include <QDir>
#include <QPushButton>
#include <QPixmap>
#include <QIcon>
#include <QSize.h>
#include "IXmlParser.h"
#include <vector>
#include <QSignalMapper>
#include <QLabel>
#include <QTextStream>
#include <QCryptographicHash>
#include <QCloseEvent>

class ExtractorGui : public QMainWindow
{
	Q_OBJECT

public:
	ExtractorGui(QWidget *parent = 0);
	~ExtractorGui();

private:
	Ui::ExtractorGuiClass ui;
	QString pptxFileName;
	QString rootPath;
	std::vector<std::wstring> textContainer;
	std::vector<std::wstring> imagesContainer;
	uint slidesAmount;
	std::auto_ptr<IXmlParser> parser;
	void closeEvent (QCloseEvent *event);

private slots:
    void on_action_Open_triggered();
	void on_action_Close_triggered();
	void displayText(const QString& pathToXml);
	void displayImages(const QString& pathToXml);
	void on_actionExport_all_triggered();
};

#endif // EXTRACTORGUI_H
