#pragma once
#include <QWidget>
#include <QLabel>

class ImageLabel : public QWidget
{
    Q_OBJECT

    public:
        explicit ImageLabel(QWidget *parent = 0);
        const QPixmap* pixmap() const;

    public slots:
        void setPixmap(const QPixmap&);

    protected:
        void resizeEvent(QResizeEvent *);
        void mousePressEvent(QMouseEvent* event);

        private slots:
            void resizeImage();

    private:
        QLabel *label;
        QPixmap *m_pixmap;
};

