#pragma once
#include "IXmlParser.h"
#include <string>
#include <vector>
#include "pugixml.hpp"

class PugiXmlParser :
	public IXmlParser
{
public:
	PugiXmlParser();
	~PugiXmlParser();
	bool parseXml(std::wstring pathToXml, std::vector<std::wstring>& textContainer,  std::vector<std::wstring>& imagesContainer);
	void getTextFromElement(pugi::xml_node element, const std::wstring& tagName, std::vector<std::wstring>& container);
	void getImagesFromXml(pugi::xml_node element, const std::wstring& tagName, std::vector<std::wstring>& _container);
};

