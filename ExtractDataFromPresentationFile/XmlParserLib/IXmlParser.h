#pragma once
#include <string>
#include <vector>

enum ParserLib
{
	tinyXml,
	pugiXml
};

class IXmlParser
{
public:
	static IXmlParser* createXmlParser(ParserLib parserLib);
	virtual bool parseXml(std::wstring pathToXml, 
		std::vector<std::wstring>& textContainer,  
		std::vector<std::wstring>& imagesContainer) = 0;
};