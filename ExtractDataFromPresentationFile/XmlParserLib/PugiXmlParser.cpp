﻿#include "PugiXmlParser.h"
#include "pugixml.hpp"
#include <iostream>


PugiXmlParser::PugiXmlParser()
{
}


PugiXmlParser::~PugiXmlParser()
{
}

bool PugiXmlParser::parseXml(std::wstring pathToXml, std::vector<std::wstring>& textContainer,  std::vector<std::wstring>& imagesContainer)
{
	if(pathToXml.find(L".rels") == std::wstring::npos)
	{
		pugi::xml_document doc;
		if(!doc.load_file(pathToXml.c_str()))
		{
			return false;
		}

		pugi::xml_node root = doc.child(L"p:sld");
		if(root == NULL)
		{
			return false;
		}

		pugi::xml_node elem = root.first_child();
		getTextFromElement(elem, L"a:t", textContainer);		
		return true;
	}
	else
	{
		pugi::xml_document doc;
		if(!doc.load_file(pathToXml.c_str()))
		{
			return false;
		}

		pugi::xml_node root = doc.child(L"Relationships");
		if(root == NULL)
		{
			return false;
		}

		//pugi::xml_node elem = root.first_child();
		getImagesFromXml(doc, L"Relationship", imagesContainer);		
		return true;
	}
	return false;
}

void PugiXmlParser::getTextFromElement(pugi::xml_node element, const std::wstring& tagName, std::vector<std::wstring>& container)
{
	std::wstring elemName = element.name();
	if(element.first_child() == 0 && element.next_sibling() == 0)
	{
		return;
	}

	if(element.first_child())
	{
		pugi::xml_node tmp = element.first_child();
		elemName = tmp.name();
		std::string comparison(tagName.begin(), tagName.end()); 
		if (elemName == tagName)
		{
			if(tmp.first_child().value() != NULL)
			{
				const pugi::char_t* ttt = tmp.first_child().value();
				container.push_back(tmp.first_child().value());
			}
		}
		getTextFromElement(tmp, tagName, container);
	}

	if(element.next_sibling())
	{
		pugi::xml_node tmp = element.next_sibling();
		elemName = tmp.name();
		std::string comparison(tagName.begin(), tagName.end());
		if(elemName == tagName)
		{
			if(tmp.first_child().value() != NULL)
			{
				const pugi::char_t* ttt = tmp.first_child().value();
				container.push_back(tmp.first_child().value());
			}
		}
		getTextFromElement(tmp, tagName, container);
	}
}

void PugiXmlParser::getImagesFromXml(pugi::xml_node element, const std::wstring& tagName, std::vector<std::wstring>& _container)
{
	std::wstring elemName = element.name();
	if(element.first_child() == 0 && element.next_sibling() == 0)
	{
		return;
	}

	if(element.first_child())
	{
		pugi::xml_node tmp = element.first_child();
		elemName = tmp.name(); 
		if (elemName == tagName)
		{
			if(tmp.attribute(L"Target"))
			{
				_container.push_back(tmp.attribute(L"Target").value());
			}			
		}
		getImagesFromXml(tmp, tagName, _container);
	}
	if(element.next_sibling())
	{
		pugi::xml_node tmp = element.next_sibling();
		elemName = tmp.name();
		if(elemName == tagName)
		{
			if(tmp.attribute(L"Target"))
			{
				_container.push_back(tmp.attribute(L"Target").value());
			}
		}
		getImagesFromXml(tmp, tagName, _container);
	}
}