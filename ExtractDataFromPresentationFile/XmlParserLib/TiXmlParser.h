#pragma once
#include "IXmlParser.h"
#include "tinyxml.h"

class TiXmlParser :
	public IXmlParser
{
public:
	TiXmlParser(void);
	~TiXmlParser(void);
	bool parseXml(std::wstring pathToXml, std::vector<std::wstring>& textContainer,  std::vector<std::wstring>& imagesContainer);
	void getTextFromElement(TiXmlElement* element, const std::wstring& tagName, std::vector<std::wstring>& container);
    void getImagesFromXml(TiXmlElement* element, const std::wstring& tagName, std::vector<std::wstring>& _container);
	std::wstring string2Wstring(const std::string& str);
	std::string wstring2String(const std::wstring& wstr);
};

