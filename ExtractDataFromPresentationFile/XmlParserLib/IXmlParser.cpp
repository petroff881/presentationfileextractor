#include "IXmlParser.h"
#include "TiXmlParser.h"
#include "PugiXmlParser.h"

IXmlParser* IXmlParser::createXmlParser(ParserLib parserLib)
{
	switch(parserLib)
	{
	case tinyXml:
		return new TiXmlParser; 
		break;
	case pugiXml:
		return new PugiXmlParser;
		break;
	default:
		throw std::exception("Not such parser");
	}
}