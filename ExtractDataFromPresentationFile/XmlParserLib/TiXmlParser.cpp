#include "TiXmlParser.h"
#include <iostream>

TiXmlParser::TiXmlParser(void)
{
}


TiXmlParser::~TiXmlParser(void)
{
}

bool TiXmlParser::parseXml(std::wstring pathToXml, std::vector<std::wstring>& textContainer,  std::vector<std::wstring>& imagesContainer)
{
	if((wstring2String(pathToXml)).find(".rels") == std::wstring::npos)
	{
		std::string pathStr = wstring2String(pathToXml); //[*]
		const char* path = pathStr.c_str();
		TiXmlDocument doc;
		if(!doc.LoadFile(path))
		{
			std::cerr << doc.ErrorDesc() << std::endl;
			return false;
		}

		TiXmlElement* root = doc.FirstChildElement();
		if(root == NULL)
		{
			std::cerr << "Failed to load file: No root element." << std::endl;
			doc.Clear();
			return false;
		}
		TiXmlElement* elem = root->FirstChildElement();
		getTextFromElement(elem, L"a:t", textContainer);
		// TODO: RAII
		doc.Clear();  
		return true;
	}
	else
	{
		std::string pathStr = wstring2String(pathToXml);
		const char* path = pathStr.c_str();
		TiXmlDocument doc;
		if(!doc.LoadFile(path))
		{
			std::cerr << doc.ErrorDesc() << std::endl;
			return false;
		}
		TiXmlElement* root = doc.FirstChildElement();
		if(root == NULL)
		{
			std::cerr << "Failed to load file: No root element." << std::endl;
			doc.Clear();
			return false;
		}
		TiXmlElement* elem = root;
		getImagesFromXml(elem, L"Relationship", imagesContainer);
		doc.Clear();
		return true;
	}
	return false;
}

void TiXmlParser::getTextFromElement(TiXmlElement* element, const std::wstring& tagName, std::vector<std::wstring>& container)
{
	std::wstring elemName = string2Wstring(element -> Value());
	if(!element -> FirstChildElement() && !element -> NextSiblingElement())
	{
		return;
	}

	if(element -> FirstChildElement())
	{
		TiXmlElement* tmp = element -> FirstChildElement();
		elemName = string2Wstring(tmp -> Value());
		if(elemName == tagName)
		{
			if(tmp -> GetText() != NULL)
			{
				container.push_back(string2Wstring(element -> GetText()));
			}
		}
		getTextFromElement(tmp, tagName, container);
	}

	if(element -> NextSiblingElement())
	{
		TiXmlElement* tmp = element -> NextSiblingElement();
		elemName = string2Wstring(tmp -> Value());
		if(elemName == tagName)
		{
			if(tmp -> GetText() != NULL)
			{
				container.push_back(string2Wstring(tmp -> GetText()));
			}			
		}
		getTextFromElement(tmp, tagName, container);
	}
}

std::wstring TiXmlParser::string2Wstring(const std::string& str)
{
	std::wstring wsTmp(str.begin(), str.end());
	return wsTmp;
}

std::string TiXmlParser::wstring2String(const std::wstring& wstr)
{
	std::string sTmp(wstr.begin(), wstr.end());
	return sTmp;
}

void TiXmlParser::getImagesFromXml(TiXmlElement* element, const std::wstring& tagName, std::vector<std::wstring>& _container)
{
	std::wstring elemName = string2Wstring(element -> Value());
	if(!element -> FirstChildElement() && !element -> NextSiblingElement())
	{
		return;
	}

	if(element -> FirstChildElement())
	{
		TiXmlElement* tmp = element -> FirstChildElement();
		elemName = string2Wstring(tmp -> Value());
		if(elemName == tagName)
		{
			std::string str(tmp->Attribute("Target"));
			if(str.length())
			{
				_container.push_back(string2Wstring(str));
			}			
		}
		getImagesFromXml(tmp, tagName, _container);
	}

	if(element -> NextSiblingElement())
	{
		TiXmlElement* tmp = element -> NextSiblingElement();
		elemName = string2Wstring(tmp -> Value());
		if(elemName == tagName)
		{
			std::string str(tmp->Attribute("Target"));
			if(str.length())
			{
				_container.push_back(string2Wstring(str));
			}
		}
		getImagesFromXml(tmp, tagName, _container);
	}
}